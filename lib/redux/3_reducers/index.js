"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _tweetsReducer = require("./tweetsReducer");

var _tweetsReducer2 = _interopRequireDefault(_tweetsReducer);

var _userReducer = require("./userReducer");

var _userReducer2 = _interopRequireDefault(_userReducer);

var _pageDataReducer = require("./pageDataReducer");

var _pageDataReducer2 = _interopRequireDefault(_pageDataReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (0, _redux.combineReducers)({
  tweets: _tweetsReducer2.default,
  user: _userReducer2.default,
  pageData: _pageDataReducer2.default
});

exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(_default, "default", "app/frontend/redux/3_reducers/index.js");
}();

;