"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = reducer;
function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    user: {
      id: null,
      name: null,
      age: null
    },
    fetching: false,
    fetched: false,
    error: null
  };
  var action = arguments[1];


  switch (action.type) {
    case "FETCH_USER":
      {
        return _extends({}, state, { fetching: true });
      }
    case "FETCH_USER_REJECTED":
      {
        return _extends({}, state, { fetching: false, error: action.payload });
      }
    case "FETCH_USER_FULFILLED":
      {
        return _extends({}, state, {
          fetching: false,
          fetched: true,
          user: action.payload
        });
      }
    case "SET_USER_NAME":
      {
        return _extends({}, state, {
          user: _extends({}, state.user, { name: action.payload })
        });
      }
    case "SET_USER_AGE":
      {
        return _extends({}, state, {
          user: _extends({}, state.user, { age: action.payload })
        });
      }
  }

  return state;
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(reducer, "reducer", "app/frontend/redux/3_reducers/userReducer.js");
}();

;