"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = reducer;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    tweets: [],
    fetching: false,
    fetched: false,
    error: null
  };
  var action = arguments[1];


  switch (action.type) {
    case "FETCH_TWEETS":
      {
        return _extends({}, state, { fetching: true });
      }
    case "FETCH_TWEETS_REJECTED":
      {
        return _extends({}, state, { fetching: false, error: action.payload });
      }
    case "FETCH_TWEETS_FULFILLED":
      {
        return _extends({}, state, {
          fetching: false,
          fetched: true,
          tweets: action.payload
        });
      }
    case "ADD_TWEET":
      {
        return _extends({}, state, {
          tweets: [].concat(_toConsumableArray(state.tweets), [action.payload])
        });
      }
    case "UPDATE_TWEET":
      {
        var _action$payload = action.payload,
            id = _action$payload.id,
            text = _action$payload.text;

        var newTweets = [].concat(_toConsumableArray(state.tweets));
        var tweetToUpdate = newTweets.findIndex(function (tweet) {
          return tweet.id === id;
        });
        newTweets[tweetToUpdate] = action.payload;

        return _extends({}, state, {
          tweets: newTweets
        });
      }
    case "DELETE_TWEET":
      {
        return _extends({}, state, {
          tweets: state.tweets.filter(function (tweet) {
            return tweet.id !== action.payload;
          })
        });
      }
  }

  return state;
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(reducer, "reducer", "app/frontend/redux/3_reducers/tweetsReducer.js");
}();

;