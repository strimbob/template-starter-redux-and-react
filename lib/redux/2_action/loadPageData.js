"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPageData = getPageData;

require("whatwg-fetch");

function getPageData(path) {
  return function (dispatch) {
    dispatch({ type: "FETCHING_PAGE" });
    fetch(path).then(function (response) {
      if (!response.ok) {
        dispatch({ type: "FETCHING_ERROR" });
        throw Error(response.statusText);
      }
      return response;
    }).then(function (response) {
      return response.json();
    }).then(function (items) {
      return dispatch({ type: "FETCHED_PAGED", payload: items });
    });
  };
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(getPageData, "getPageData", "app/frontend/redux/2_action/loadPageData.js");
}();

;