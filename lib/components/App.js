"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _userActions = require("../redux/2_action/userActions.js");

var _tweetsActions = require("../redux/2_action/tweetsActions.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Layout = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    user: store.user.user,
    userFetched: store.user.fetched,
    tweets: store.tweets.tweets
  };
}), _dec(_class = function (_React$Component) {
  _inherits(Layout, _React$Component);

  function Layout() {
    _classCallCheck(this, Layout);

    return _possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).apply(this, arguments));
  }

  _createClass(Layout, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      this.props.dispatch((0, _userActions.fetchUser)());
    }
  }, {
    key: "fetchTweets",
    value: function fetchTweets() {
      this.props.dispatch((0, _tweetsActions.fetchTweets)());
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          user = _props.user,
          tweets = _props.tweets;


      if (!tweets.length) {
        return _react2.default.createElement(
          "button",
          { onClick: this.fetchTweets.bind(this) },
          "load tweets"
        );
      }

      var mappedTweets = tweets.map(function (tweet) {
        return _react2.default.createElement(
          "li",
          { key: tweet.id },
          tweet.text
        );
      });

      return _react2.default.createElement(
        "div",
        null,
        _react2.default.createElement(
          "h1",
          null,
          user.name
        ),
        _react2.default.createElement(
          "ul",
          null,
          mappedTweets
        )
      );
    }
  }]);

  return Layout;
}(_react2.default.Component)) || _class);
exports.default = Layout;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Layout, "Layout", "app/frontend/components/App.js");
}();

;