"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _userActions = require("../redux/2_action/userActions.js");

var _tweetsActions = require("../redux/2_action/tweetsActions.js");

var _loadPageData = require("../redux/2_action/loadPageData.js");

var _title = require("./atom/title.js");

var _title2 = _interopRequireDefault(_title);

var _subtitle = require("./atom/subtitle.js");

var _subtitle2 = _interopRequireDefault(_subtitle);

var _textBody = require("./atom/textBody.js");

var _textBody2 = _interopRequireDefault(_textBody);

var _image = require("./atom/image.js");

var _image2 = _interopRequireDefault(_image);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _className = "pageData";
var Test = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    user: store.user.user,
    userFetched: store.user.fetched,
    tweets: store.tweets.tweets,
    pageData: store.pageData.pageData

  };
}), _dec(_class = function (_React$Component) {
  _inherits(Test, _React$Component);

  function Test() {
    _classCallCheck(this, Test);

    return _possibleConstructorReturn(this, (Test.__proto__ || Object.getPrototypeOf(Test)).apply(this, arguments));
  }

  _createClass(Test, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      console.log(this.props.location);

      this.props.pageData.className = "pageData";
      this.props.dispatch((0, _userActions.fetchUser)());
      this.props.dispatch((0, _loadPageData.getPageData)("./static/data/story/Aquapretpark.json"));
      console.log("this this it ??" + this.props.pageData.title);
    }
  }, {
    key: "fetchTweets",
    value: function fetchTweets() {
      this.props.dispatch((0, _loadPageData.getPageData)("./static/data/story/Aquapretpark.json"));
      console.log("this this it ??" + this.props.pageData.title);
    }
  }, {
    key: "render",
    value: function render() {
      var style = {
        color: "red"
      };
      if (this.props.pageData.title === null) {
        return _react2.default.createElement(
          "div",
          null,
          " LOADING "
        );
      }
      return _react2.default.createElement(
        "div",
        { className: _className },
        _react2.default.createElement(_title2.default, { className: _className, data: this.props.pageData }),
        _react2.default.createElement(_subtitle2.default, { className: _className, data: this.props.pageData }),
        _react2.default.createElement(_textBody2.default, { className: _className, data: this.props.pageData }),
        _react2.default.createElement(_image2.default, { className: _className, data: this.props.pageData })
      );
    }
  }]);

  return Test;
}(_react2.default.Component)) || _class);
exports.default = Test;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(_className, "_className", "app/frontend/components/loaddata.js");

  __REACT_HOT_LOADER__.register(Test, "Test", "app/frontend/components/loaddata.js");
}();

;