"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _loadPageData = require("../redux/2_action/loadPageData.js");

var _title = require("./atom/title.js");

var _title2 = _interopRequireDefault(_title);

var _subtitle = require("./atom/subtitle.js");

var _subtitle2 = _interopRequireDefault(_subtitle);

var _textBody = require("./atom/textBody.js");

var _textBody2 = _interopRequireDefault(_textBody);

var _image = require("./atom/image.js");

var _image2 = _interopRequireDefault(_image);

var _spinner = require("./atom/spinner.js");

var _spinner2 = _interopRequireDefault(_spinner);

var _list = require("./atom/list.js");

var _list2 = _interopRequireDefault(_list);

var _Menu = require("../../lists/Menu.js");

var _Menu2 = _interopRequireDefault(_Menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _className = "pageData";
var _classNameMenu = "menu";

var PathToContent = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    other: store.pageData,
    pageData: store.pageData.pageData
  };
}), _dec(_class = function (_React$Component) {
  _inherits(PathToContent, _React$Component);

  function PathToContent() {
    _classCallCheck(this, PathToContent);

    return _possibleConstructorReturn(this, (PathToContent.__proto__ || Object.getPrototypeOf(PathToContent)).apply(this, arguments));
  }

  _createClass(PathToContent, [{
    key: "componentWillMount",
    value: function componentWillMount() {

      this.props.pageData.className = "pageData";
      this.props.dispatch((0, _loadPageData.getPageData)("./static/data/story" + this.props.location.pathname + ".json"));
    }
  }, {
    key: "render",
    value: function render() {
      if (this.props.other.fetching) {

        return _react2.default.createElement(
          "div",
          null,
          _react2.default.createElement(_list2.default, { className: _classNameMenu, list: _Menu2.default }),
          "loading",
          _react2.default.createElement(_spinner2.default, null)
        );
      } else if (this.props.other.fetched) {
        return _react2.default.createElement(
          "div",
          { className: _className },
          _react2.default.createElement(_list2.default, { className: _classNameMenu, list: _Menu2.default }),
          _react2.default.createElement(_title2.default, { className: _className, data: this.props.pageData }),
          _react2.default.createElement(_subtitle2.default, { className: _className, data: this.props.pageData }),
          _react2.default.createElement(_textBody2.default, { className: _className, data: this.props.pageData }),
          _react2.default.createElement(_image2.default, { className: _className, data: this.props.pageData })
        );
      } else if (this.props.other.error != null) {
        return _react2.default.createElement(
          "div",
          null,
          _react2.default.createElement(_list2.default, { className: _classNameMenu, list: _Menu2.default }),
          "not found  hi "
        );
      } else {
        return _react2.default.createElement("div", null);
      }
    }
  }]);

  return PathToContent;
}(_react2.default.Component)) || _class);
exports.default = PathToContent;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(_className, "_className", "app/frontend/components/pathToContent.js");

  __REACT_HOT_LOADER__.register(_classNameMenu, "_classNameMenu", "app/frontend/components/pathToContent.js");

  __REACT_HOT_LOADER__.register(PathToContent, "PathToContent", "app/frontend/components/pathToContent.js");
}();

;