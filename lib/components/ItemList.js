'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _items = require('../actions/items');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ItemList = function (_Component) {
    _inherits(ItemList, _Component);

    function ItemList() {
        _classCallCheck(this, ItemList);

        return _possibleConstructorReturn(this, (ItemList.__proto__ || Object.getPrototypeOf(ItemList)).apply(this, arguments));
    }

    _createClass(ItemList, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            //  this.props.fetchData('http://599167402df2f40011e4929a.mockapi.io/items');
            this.props.fetchData('static/data/data.json');
        }
    }, {
        key: 'render',
        value: function render() {
            if (this.props.hasErrored) {
                return _react2.default.createElement(
                    'p',
                    null,
                    'Sorry! There was an error loading the items'
                );
            }

            if (this.props.isLoading) {
                return _react2.default.createElement(
                    'p',
                    null,
                    'Loading\u2026'
                );
            }

            return _react2.default.createElement(
                'ul',
                null,
                this.props.items.map(function (item) {
                    return _react2.default.createElement(
                        'li',
                        { key: item.headline },
                        item.headline
                    );
                })
            );
        }
    }]);

    return ItemList;
}(_react.Component);

ItemList.propTypes = {
    fetchData: _propTypes2.default.func.isRequired,
    items: _propTypes2.default.array.isRequired,
    hasErrored: _propTypes2.default.bool.isRequired,
    isLoading: _propTypes2.default.bool.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
    return {
        items: state.items,
        hasErrored: state.itemsHasErrored,
        isLoading: state.itemsIsLoading
    };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
        fetchData: function fetchData(url) {
            return dispatch((0, _items.itemsFetchData)(url));
        }
    };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ItemList);

exports.default = _default;
;

var _temp = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(ItemList, 'ItemList', 'app/frontend/components/ItemList.js');

    __REACT_HOT_LOADER__.register(mapStateToProps, 'mapStateToProps', 'app/frontend/components/ItemList.js');

    __REACT_HOT_LOADER__.register(mapDispatchToProps, 'mapDispatchToProps', 'app/frontend/components/ItemList.js');

    __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/components/ItemList.js');
}();

;