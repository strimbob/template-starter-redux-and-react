'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require('react-router-dom');

var _App = require('../components/App');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Root = function Root() {
  return _react2.default.createElement(
    _reactRouterDom.BrowserRouter,
    { basename: process.env.PUBLIC_URL },
    _react2.default.createElement(
      _reactRouterDom.Switch,
      null,
      _react2.default.createElement(_reactRouterDom.Route, { path: '/', component: _App2.default })
    )
  );
};

var _default = Root;
exports.default = _default;

// <Route exact path="/" component={App} />
//  <Route exact path="/App" component={Test} />
//  <Route exact path="/Layout" component={Layout} />
//  <Route path="/test" component={PathToContent} />
//  <Route exact path="/layout:" component={Layout} />
// <Route path="/" component={PathToContent} />

;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Root, 'Root', 'app/frontend/router/index.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/router/index.js');
}();

;