export default function reducer(state={
    pageData: {
      id: null,
      title: null,
      subtitle: null,
      textBody: [[]],
      imgPhone: null,
      imgDtop: null,
      imgPhoneS: null,
      imgDtopS: null,
      hero: null,
      iframes:[],
      pageImage: null,
      numberImage: null,
    },
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type){
      case "FETCHING_PAGE":{
      return {...state,fetching: true}
      }
      case "FETCHED_PAGED":{
      return {...state, fetching: false, fetched: true,pageData: action.payload}
      }
      case "FETCHING_ERROR":{
      return {...state, fetching: false, fetched: false, error: "page not found 404 "}
      }
    }
    return state
}
