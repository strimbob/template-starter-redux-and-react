import { combineReducers } from "redux"

import tweets from "./tweetsReducer"
import user from "./userReducer"
import pageData from "./pageDataReducer"
import { reducer as responsive } from 'react-responsive-redux'
export default combineReducers({
  tweets,
  user,
  pageData,
  responsive
})
