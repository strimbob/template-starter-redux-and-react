export const download = {
title: "Create muisc with iris",
subtile: "mac osx",
button: "DOWNLOAD IRIS",
body: "Iris frees you from the constraints of a timeline, enables you to jam, play, create structures on the fly in a repeatable way.",
link: "../static/svg/danceWebreadyCom.svg"

}
