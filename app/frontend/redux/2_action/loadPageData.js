import 'whatwg-fetch'
export function getPageData(path) {
  return function(dispatch) {
    dispatch({type: "FETCHING_PAGE"});
    fetch(path)
        .then((response) => {
            if (!response.ok) {
              dispatch({type: "FETCHING_ERROR"})
                throw Error(response.statusText);
            }
            return response;
        })
        .then((response) => response.json())
        .then((items) => dispatch({type: "FETCHED_PAGED", payload: items}))
  }
}
