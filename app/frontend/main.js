import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/1_store/store.js';
import { AppContainer } from 'react-hot-loader';
import Root from './router/index';

const render = (Component) => {
  ReactDOM.render(
        <Provider store={store}>
          <AppContainer>
            <Component />
          </AppContainer>
        </Provider>,
    document.getElementById('root'),
  );
};

render(Root);

if (module.hot) {
  module.hot.accept('./router/index', () => {
    const newApp = require('./router/index').default;
    render(newApp);
  });
}
