import React from "react"
import {PhoneScreen, TabletScreen, DesktopScreen } from 'react-responsive-redux'
import {indexPage} from '../../redux/data/indexPage.js'
import AddressBar from '../modular/addressBar.js'
import YoutubeLazy from '../organisms/youTubeLazyLoader.js'

export default class IndexMain extends React.Component {
  render() {

    return (
    <div>
      <PhoneScreen>
  <div><h1>You are a PhoneScreen device</h1></div>
</PhoneScreen>
<TabletScreen>
<div><h1>You are a TabletScreen device</h1></div>
</TabletScreen>
<DesktopScreen>
<div><h1>You are a DesktopScreen device</h1></div>
</DesktopScreen>



        <AddressBar history={this.props.history} />
      <div className={"indexPage"}>
        <div className={"indexPage__titleTop"}>
          <h1>{indexPage[0].title}</h1></div>
        <div className={"indexPage__bodyTop"}>
          <h3>{indexPage[0].body}</h3></div>
        <img className={"indexPage__Svg"}
          src={__dirname+"../static/svg/laptopSvg1.svg"}></img>
        <div className={"indexPage__top"}></div>
        <div className={"indexPage__bottom"}></div>
        <div className={"indexPage__titleMiddle"}>
          <h1>{indexPage[1].title}</h1></div>
        <div className={"indexPage__bodyMiddle"}>
          <h3>{indexPage[1].body}</h3></div>
            <YoutubeLazy className={"indexPage__VideoMiddle"} link={indexPage[1].link} />

        <div className={"indexPage__titleBottom"}>
          <h1>{indexPage[2].title}</h1></div>
          <YoutubeLazy className={"indexPage__VideoBottom"} link={indexPage[2].link} />

        <div className={"indexPage__redBox"}></div>
      </div>
    </div>
    )
  }
  _onReady(event) {}
}
