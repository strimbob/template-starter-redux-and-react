import React from "react"
import { connect } from "react-redux"
import {download} from '../../redux/data/download.js'
import AddressBar from '../modular/addressBar.js'
@connect((store) => {
  return {
  };
})
export default class Download extends React.Component {

clickHanlder(){
  console.log("download Iris");
}
  render() {
    return (
    <div>
          <AddressBar history={this.props.history} />
      <div className={"download"}>
        <img className={"download__svg"}  src={__dirname+download.link}></img>
        <div className={"download__title"}>   <h1>{download.title}</h1></div>
        <div className={"download__subtile"}> <h2>{download.subtile}</h2></div>
        <div className={"download__button"} onClick={this.clickHanlder.bind(this)}>
        <h4>{download.button}</h4></div>
        <div className={"download__body"}>    <h3>{download.body}</h3></div>
      </div>
    </div>
  )
  }
}
