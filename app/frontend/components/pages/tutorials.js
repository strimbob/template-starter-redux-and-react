import React from "react"
import { connect } from "react-redux"
import {tutorials} from '../../redux/data/tutorials.js'
import AddressBar from '../modular/addressBar.js'
import YoutubeLazy from '../organisms/youTubeLazyLoader.js'

@connect((store) => {
  return {
  };
})

export default class Tutorials extends React.Component {
  render() {

const allTuts = tutorials.playlist.map((list) =>
    <div className={"tuts__playlist"} key={list.title}>
    <div className={"tuts__playlist--title"}><h2>{list.title}</h2></div>
      <YoutubeLazy
        className={"tuts__video"}
        link={list.link}
        />
    </div>)

    return (
    <div>
        <AddressBar history={this.props.history} />
      <div className={"tuts"} >
        <div className={"tuts__title"}><h1>{tutorials.title}</h1></div>
        <div className={"tuts__body"}><h3>{tutorials.body}</h3></div>
        {allTuts}
      </div>
    </div>
  )
  }
}
