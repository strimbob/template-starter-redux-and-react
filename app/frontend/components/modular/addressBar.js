import React from "react"

import {addressBar} from '../../redux/data/addressBar.js'

export default class AddressBar extends React.Component {

clickHandler(path){
 this.props.history.push(path)
}

  render() {
    let namelist = addressBar.map((_addressBar) =>
      <div className={"addressBar__left"}
        onClick={this.clickHandler.bind(this,_addressBar.link)}
        key={_addressBar.name}> {_addressBar.name}
      </div>)

    return (
    <div className={"addressBar"}>
      {namelist}
      <div className={" addressBar__Right"}>join up</div>
    </div>
  )
  }
}
