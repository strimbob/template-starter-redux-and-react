import Download from '../components/pages/download.js'
import IndexMain from '../components/pages/index.js'
import Tutorials from '../components/pages/tutorials.js'
import AppRoot from './app-root';
const routes = [
  { component: AppRoot,
    routes: [
      { path: '/',
        exact: true,
        component: IndexMain
      },
      { path: '/Download',
        component: Download
      },
      { path: '/Tutorials',
        component: Tutorials
      }
    ]
  }
];
export default routes;
