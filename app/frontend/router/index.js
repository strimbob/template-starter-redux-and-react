import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import routes from './routes';
/* @flow */
const Root = () => {
  return (
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    {renderRoutes(routes)}
      </BrowserRouter>
  );
};

export default Root;
