
import * as functions from 'firebase-functions';
const firebase = require('firebase-admin');
const firebaseApp = firebase.initializeApp(functions.config().firebase);
var database = firebase.database();
import React from 'react';
import {renderToString} from 'react-dom/server';
import StaticRouter from 'react-router-dom/StaticRouter';
import { renderRoutes } from 'react-router-config';
import express from 'express';
import fs from 'fs';

var path = require("path");

import routes from './frontend/router/routes.js';
import IndexMain from './frontend/components/pages/index.js'
import { Provider } from 'react-redux';
import store from './frontend/redux/1_store/store.js';
import { setMobileDetect, mobileParser } from 'react-responsive-redux'

const app = express();

const assets = express.static(path.join(__dirname, '../public'));
app.use(assets);
const _index = fs.readFileSync(path.resolve(__dirname, './public', 'nerves.html'),'utf8');


function renderFullPage(html, preloadedState) {
  let htmlFinal = _index.replace("<!--- ::APP::----->",html);
  let store_init =  `<script> window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')} </script>`
  htmlFinal = htmlFinal.replace("<!--- ::store::----->",store_init);
  return htmlFinal;
}

app.get('*', function (request, response){

  let context = {};
  const { dispatch } = store;

  // do our mobile detection
  const mobileDetect = mobileParser(request)
dispatch(setMobileDetect(mobileDetect))

  const content = renderToString(
    <Provider store={store}>
    <StaticRouter location={request.url} context={context}>
      {renderRoutes(routes)}
    </StaticRouter>
    </Provider>
  );

  const htmlFinal = _index.replace("<!--- ::APP::----->",content);
  const preloadedState = store.getState()
  response.send(renderFullPage(content, preloadedState))
})

export let daBrain = functions.https.onRequest(app);
