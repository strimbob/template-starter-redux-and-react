var htmlToText = require('html-to-text');
require('google-sheets-api');
var fs = require('fs');
var Documents = require('google-documents-api');
var serviceEmail = 'megabenzine@megabenzine.iam.gserviceaccount.com';
var serviceKey = fs.readFileSync(__dirname + '/keys/sheets.pem').toString();
var Jimp = require("jimp");
var storysf= [];
var mm =  '';
var Promise = require("promise");
var fs = require('fs'),
    request = require('request');
var jsonfile = require('jsonfile')
var mkdirp = require('mkdirp');
import downimage from './imageProssing.js'
//_________________________________________________________________
function getText(_d,_t){
return _d.split(_t)[1].split("#_")[0].replace(/^\s*\n/gm, "").split("\n")[0];
}
//_________________________________________________________________
function getMainText(data,type){
var main =[]
var res = data.split(type);
  for(var q = 0; q<res.length-1; q++){
    var _tempHeadline = res[q+1].split("#_");
    var resultString = _tempHeadline[0].replace(/^\s*\n/gm, "");
    var splitted = resultString.split("\n");
    main.push(splitted);
  }
return main;
}
function getIframes(data,type){
  var main =[]
  var res = data.split(type);
      if(res.length != 0){
    for(var q = 0; q<res.length-1; q++){
      var _tempHeadline = res[q+1].split("#_");
      var resultString = _tempHeadline[0].replace(/^\s*\n/gm, "");
      var splitted = resultString.replace("\n",'');
      main.push(splitted);
    }
    return main;
  }else{
    return "empty";
  }
}
//_________________________________________________________________
function getHeadline(data){

var res = data.split("#_Headline_#");
var _tempHeadline = res[1].split("#_");
var resultString = _tempHeadline[0].replace(/^\s*\n/gm, "");
var splitted = resultString.split("\n");
}

//_________________________________________________________________
function getAllImage(data,fileName){
var res = data.split("#_image_#");
getImagePaths(res[1],fileName);
}

//_________________________________________________________________
function getImagePaths(data,fileName){
var  res = data.split("[https://");
var _imagePath = [];
  for(var q = 0; q<res.length-1; q++){
    var _tempImage = res[q+1].split("]");
_imagePath.push(_tempImage[0]);
 downimage.downimage('https://'+_tempImage[0],fileName+q);
  }
}
//_________________________________________________________________
module.exports = {
   getStory: function(documentId,firebase){
      fs.unlinkSync('../public/static/data/index.json');
        var sideshow = {slide: "slide"}
     jsonfile.writeFile('../public/static/data/index.json',sideshow, function (err) {
       console.error(err)
     })
   console.log("making a new index.json");

  var docs = new Documents({ email: serviceEmail, key: serviceKey });
  docs.getDocumentHtml(documentId).then(function(htmlContent) {
    var text = htmlToText.fromString(htmlContent, {
        wordwrap: 130
    });

var section = getText(text,"#_section_#").toLowerCase();
var headline = getText(text,"#_Headline_#");
var tag = getText(text,"#_Tag_#");
var main = getMainText(text,"#_Main_#");
var iframes = getIframes(text,"#_iframes_#");
var  numberImage = text.split("[https://").length-2;
 getImagePaths(text,headline);
writeUserData(documentId,documentId,"true",firebase);
var phoneImg = './static/images/phone/'+headline+'0'+'.jpg';
var phoneSmallImg = './static/images/phoneSmall/'+headline+'0'+'.jpg';
var desktopImg = './static/images/desktop/'+headline+'0'+'.jpg';
var desktopSmallImg = './static/images/desktopSmall/'+headline+'0'+'.jpg';
var hero = './static/images/hero/'+headline+'0'+'.jpg';
var pageImage = './static/images/pageImage/'+headline+'0'+'.jpg';


var phoneImgWebp = './static/images/phone/'+headline+'0'+'.webp';
var phoneSmallImgWebp = './static/images/phoneSmall/'+headline+'0'+'.webp';
var desktopImgWebp = './static/images/desktop/'+headline+'0'+'.webp';
var desktopSmallImgWebp = './static/images/desktopSmall/'+headline+'0'+'.webp';
var heroWebp = './static/images/hero/'+headline+'0'+'.webp';
var pageImageWebp = './static/images/pageImage/'+headline+'0'+'.webp';


writeJson(section,documentId,headline,tag,main,phoneImg,desktopImg,firebase,phoneSmallImg,desktopSmallImg,hero,iframes,pageImage,numberImage,
phoneImgWebp,phoneSmallImgWebp,desktopImgWebp,desktopSmallImgWebp,heroWebp,pageImageWebp);
//writeStroy(section,documentId,headline,tag,main,phoneImg,desktopImg,firebase,phoneSmallImg,desktopSmallImg,hero,iframes,pageImage,numberImage);

    });
  }
};

function removeData(firebase,userId){
  firebase.database().ref.remove('storys/' + userId);
}

function writeUserData(userId, _id, bool,firebase) {
  firebase.database().ref('storys/' + userId).set({
    id: _id,
    isConplete: bool,
  });
}

function writeStroy(section,_id, headline, tag,main,phone,dtop,firebase,phonesmall,desktopSmall,hero,iframes,pageImage,numberImage) {
  firebase.database().ref(section+'/' + headline).set({
    id: _id,
    title: headline,
    subtitle: tag,
    textBody: main,
    imgPhone: phone,
    imgDtop : dtop,
    imgPhoneS : phonesmall,
    imgDtopS : desktopSmall,
    hero : hero,
    iframes: iframes,
    pageImage: pageImage,
    numberImage: numberImage
  });
  console.log('written data base for ' + headline)
}

function writeJson(section,_id, headline, tag,main,phone,dtop,firebase,phonesmall,desktopSmall,hero,iframes,pageImage,numberImage,
phoneImgWebp,phoneSmallImgWebp,desktopImgWebp,desktopSmallImgWebp,heroWebp,pageImageWebp) {

console.log("saveing josn")
  var file = '../public/static/data/'+section+'/'+headline+'.json'
  var obj = {    id: _id,
      title: headline,
      subtitle: tag,
      textBody: main,
      imgPhone: phone,
      imgDtop : dtop,
      imgPhoneS : phonesmall,
      imgDtopS : desktopSmall,
      hero : hero,
      iframes: iframes,
      pageImage: pageImage,
      numberImage: numberImage,
      phoneImgWebp: phoneImgWebp,
      phoneSmallImgWebp: phoneSmallImgWebp,
      desktopImgWebp : desktopImgWebp,
      desktopSmallImgWebp : desktopSmallImgWebp,
      heroWebp : heroWebp ,
      pageImageWebp : pageImageWebp

    }

console.log("afuck aaaaaddddddddddddddddddddddddddddddddddd!");

//       fs.writeFile(__dirname + '/../../../../public/file.txt', obj, function(err) {
//     if(err) {
//         return console.log(err);
//     }  console.log("The file was saved!");
// });


mkdirp('../public/static/data/story', function (err) {
    if (err) console.error(err)
    else console.log('pow!')
});

var file = '/tmp/mayAlreadyExistedData.json'
var index = {headline: headline}

jsonfile.writeFile('../public/static/data/index.json', index, {flag: 'a'}, function (err) {
  console.error(err)
})


  jsonfile.writeFile('../public/static/data/story/'+headline+'.json', obj, function (err) {
    console.error(err)
  })
}
