var htmlToText = require('html-to-text');
require('google-sheets-api');
var fs = require('fs');
// const webConvert =  require('./convetToWebp.js')
import WebConvert from './convetToWebp.js'
var Jimp = require("jimp");

module.exports = {
   downimage: function (uri, filename){
Jimp.read(uri).then(function (image) {
image.cover(1200,800 ).quality(90).write('../public/static/images/hero/'+filename+'.jpg')
image.cover(1000, 800).quality(90).write('../public/static/images/pageImage/'+filename+'.jpg') // save
image.cover(500,  400).quality(90).write('../public/static/images/phone/'+filename+'.jpg') // save
image.cover(400,  125).quality(90).write('../public/static/images/desktop/'+filename+'.jpg') // save
image.cover(269,  400).quality(90).write('../public/static/images/phoneSmall/'+filename+'.jpg') // save
image.cover(215,  125).quality(90).write('../public/static/images/desktopSmall/'+filename+'.jpg') // save
console.log('finnsiehed saveing image for ' + filename);
}).catch(function (err) {
  console.log("image prossing error");
    console.log(err);
});
WebConvert.WebConvert('../public/static/images/hero/'+filename);
WebConvert.WebConvert('../public/static/images/pageImage/'+filename);
WebConvert.WebConvert('../public/static/images/phone/'+filename);
WebConvert.WebConvert('../public/static/images/desktop/'+filename);
WebConvert.WebConvert('../public/static/images/phoneSmall/'+filename);
WebConvert.WebConvert('../public/static/images/desktopSmall/'+filename);

// webp.cwebp('../public/static/images/hero/'+filename+'.jpg','../public/static/images/hero/'+filename+'.webp',"-q 80",function(status){
//   	console.log(status);
//   });
}

};
