__dirname = '';
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _tutorials = require("../../redux/data/tutorials.js");

var _addressBar = require("../modular/addressBar.js");

var _addressBar2 = _interopRequireDefault(_addressBar);

var _youTubeLazyLoader = require("../organisms/youTubeLazyLoader.js");

var _youTubeLazyLoader2 = _interopRequireDefault(_youTubeLazyLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tutorials = (_dec = (0, _reactRedux.connect)(function (store) {
  return {};
}), _dec(_class = function (_React$Component) {
  _inherits(Tutorials, _React$Component);

  function Tutorials() {
    _classCallCheck(this, Tutorials);

    return _possibleConstructorReturn(this, (Tutorials.__proto__ || Object.getPrototypeOf(Tutorials)).apply(this, arguments));
  }

  _createClass(Tutorials, [{
    key: "render",
    value: function render() {

      var allTuts = _tutorials.tutorials.playlist.map(function (list) {
        return _react2.default.createElement(
          "div",
          { className: "tuts__playlist", key: list.title },
          _react2.default.createElement(
            "div",
            { className: "tuts__playlist--title" },
            _react2.default.createElement(
              "h2",
              null,
              list.title
            )
          ),
          _react2.default.createElement(_youTubeLazyLoader2.default, {
            className: "tuts__video",
            link: list.link
          })
        );
      });

      return _react2.default.createElement(
        "div",
        null,
        _react2.default.createElement(_addressBar2.default, { history: this.props.history }),
        _react2.default.createElement(
          "div",
          { className: "tuts" },
          _react2.default.createElement(
            "div",
            { className: "tuts__title" },
            _react2.default.createElement(
              "h1",
              null,
              _tutorials.tutorials.title
            )
          ),
          _react2.default.createElement(
            "div",
            { className: "tuts__body" },
            _react2.default.createElement(
              "h3",
              null,
              _tutorials.tutorials.body
            )
          ),
          allTuts
        )
      );
    }
  }]);

  return Tutorials;
}(_react2.default.Component)) || _class);
exports.default = Tutorials;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Tutorials, "Tutorials", "app/frontend/components/pages/tutorials.js");
}();

;