__dirname = '';
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactResponsiveRedux = require('react-responsive-redux');

var _indexPage = require('../../redux/data/indexPage.js');

var _addressBar = require('../modular/addressBar.js');

var _addressBar2 = _interopRequireDefault(_addressBar);

var _youTubeLazyLoader = require('../organisms/youTubeLazyLoader.js');

var _youTubeLazyLoader2 = _interopRequireDefault(_youTubeLazyLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IndexMain = function (_React$Component) {
  _inherits(IndexMain, _React$Component);

  function IndexMain() {
    _classCallCheck(this, IndexMain);

    return _possibleConstructorReturn(this, (IndexMain.__proto__ || Object.getPrototypeOf(IndexMain)).apply(this, arguments));
  }

  _createClass(IndexMain, [{
    key: 'render',
    value: function render() {

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _reactResponsiveRedux.PhoneScreen,
          null,
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'h1',
              null,
              'You are a PhoneScreen device'
            )
          )
        ),
        _react2.default.createElement(
          _reactResponsiveRedux.TabletScreen,
          null,
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'h1',
              null,
              'You are a TabletScreen device'
            )
          )
        ),
        _react2.default.createElement(
          _reactResponsiveRedux.DesktopScreen,
          null,
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'h1',
              null,
              'You are a DesktopScreen device'
            )
          )
        ),
        _react2.default.createElement(_addressBar2.default, { history: this.props.history }),
        _react2.default.createElement(
          'div',
          { className: "indexPage" },
          _react2.default.createElement(
            'div',
            { className: "indexPage__titleTop" },
            _react2.default.createElement(
              'h1',
              null,
              _indexPage.indexPage[0].title
            )
          ),
          _react2.default.createElement(
            'div',
            { className: "indexPage__bodyTop" },
            _react2.default.createElement(
              'h3',
              null,
              _indexPage.indexPage[0].body
            )
          ),
          _react2.default.createElement('img', { className: "indexPage__Svg",
            src: __dirname + "../static/svg/laptopSvg1.svg" }),
          _react2.default.createElement('div', { className: "indexPage__top" }),
          _react2.default.createElement('div', { className: "indexPage__bottom" }),
          _react2.default.createElement(
            'div',
            { className: "indexPage__titleMiddle" },
            _react2.default.createElement(
              'h1',
              null,
              _indexPage.indexPage[1].title
            )
          ),
          _react2.default.createElement(
            'div',
            { className: "indexPage__bodyMiddle" },
            _react2.default.createElement(
              'h3',
              null,
              _indexPage.indexPage[1].body
            )
          ),
          _react2.default.createElement(_youTubeLazyLoader2.default, { className: "indexPage__VideoMiddle", link: _indexPage.indexPage[1].link }),
          _react2.default.createElement(
            'div',
            { className: "indexPage__titleBottom" },
            _react2.default.createElement(
              'h1',
              null,
              _indexPage.indexPage[2].title
            )
          ),
          _react2.default.createElement(_youTubeLazyLoader2.default, { className: "indexPage__VideoBottom", link: _indexPage.indexPage[2].link }),
          _react2.default.createElement('div', { className: "indexPage__redBox" })
        )
      );
    }
  }, {
    key: '_onReady',
    value: function _onReady(event) {}
  }]);

  return IndexMain;
}(_react2.default.Component);

exports.default = IndexMain;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(IndexMain, 'IndexMain', 'app/frontend/components/pages/index.js');
}();

;