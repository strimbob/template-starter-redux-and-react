__dirname = '';
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _download = require("../../redux/data/download.js");

var _addressBar = require("../modular/addressBar.js");

var _addressBar2 = _interopRequireDefault(_addressBar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Download = (_dec = (0, _reactRedux.connect)(function (store) {
  return {};
}), _dec(_class = function (_React$Component) {
  _inherits(Download, _React$Component);

  function Download() {
    _classCallCheck(this, Download);

    return _possibleConstructorReturn(this, (Download.__proto__ || Object.getPrototypeOf(Download)).apply(this, arguments));
  }

  _createClass(Download, [{
    key: "clickHanlder",
    value: function clickHanlder() {
      console.log("download Iris");
    }
  }, {
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "div",
        null,
        _react2.default.createElement(_addressBar2.default, { history: this.props.history }),
        _react2.default.createElement(
          "div",
          { className: "download" },
          _react2.default.createElement("img", { className: "download__svg", src: __dirname + _download.download.link }),
          _react2.default.createElement(
            "div",
            { className: "download__title" },
            "   ",
            _react2.default.createElement(
              "h1",
              null,
              _download.download.title
            )
          ),
          _react2.default.createElement(
            "div",
            { className: "download__subtile" },
            " ",
            _react2.default.createElement(
              "h2",
              null,
              _download.download.subtile
            )
          ),
          _react2.default.createElement(
            "div",
            { className: "download__button", onClick: this.clickHanlder.bind(this) },
            _react2.default.createElement(
              "h4",
              null,
              _download.download.button
            )
          ),
          _react2.default.createElement(
            "div",
            { className: "download__body" },
            "    ",
            _react2.default.createElement(
              "h3",
              null,
              _download.download.body
            )
          )
        )
      );
    }
  }]);

  return Download;
}(_react2.default.Component)) || _class);
exports.default = Download;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Download, "Download", "app/frontend/components/pages/download.js");
}();

;