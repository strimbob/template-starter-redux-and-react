__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
__dirname = '';
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _addressBar2 = require("../../redux/data/addressBar.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AddressBar = function (_React$Component) {
  _inherits(AddressBar, _React$Component);

  function AddressBar() {
    _classCallCheck(this, AddressBar);

    return _possibleConstructorReturn(this, (AddressBar.__proto__ || Object.getPrototypeOf(AddressBar)).apply(this, arguments));
  }

  _createClass(AddressBar, [{
    key: "clickHandler",
    value: function clickHandler(path) {
      console.log(path);
      this.props.history.push(path);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var namelist = _addressBar2.addressBar.map(function (_addressBar) {
        return _react2.default.createElement(
          "div",
          { className: "addressBar__left",
            onClick: _this2.clickHandler.bind(_this2, _addressBar.link),
            key: _addressBar.name },
          " ",
          _addressBar.name
        );
      });

      return _react2.default.createElement(
        "div",
        { className: "addressBar" },
        namelist,
        _react2.default.createElement(
          "div",
          { className: " addressBar__Right" },
          "join up"
        )
      );
    }
  }]);

  return AddressBar;
}(_react2.default.Component);

exports.default = AddressBar;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(AddressBar, "AddressBar", "app/frontend/components/modular/addressBar.js");
}();

;