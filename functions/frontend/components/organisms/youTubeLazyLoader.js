__dirname = '';
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var YoutubeLazy = function (_React$Component) {
  _inherits(YoutubeLazy, _React$Component);

  function YoutubeLazy(props) {
    _classCallCheck(this, YoutubeLazy);

    var _this = _possibleConstructorReturn(this, (YoutubeLazy.__proto__ || Object.getPrototypeOf(YoutubeLazy)).call(this, props));

    _this.state = {
      showYoutube: false
    };
    return _this;
  }

  _createClass(YoutubeLazy, [{
    key: "clickHandler",
    value: function clickHandler() {
      this.setState({ showYoutube: true });
    }
  }, {
    key: "onReady",
    value: function onReady() {
      console.log("no wya!");
    }
  }, {
    key: "render",
    value: function render() {
      var style = {
        objectFit: "cover"
      };

      var styleTwo = {
        position: "absolute",
        zIndex: "9"
      };
      var className = this.props.className;
      var link = this.props.link;

      if (this.state.showYoutube) {
        return _react2.default.createElement("iframe", { className: this.props.className,
          src: "https://www.youtube.com/embed/" + this.props.link + "?rel=0&amp;showinfo=0&autoplay=1",
          frameBorder: "0", onReady: this.onReady(this), allow: "autoplay 1; encrypted-media", allowFullScreen: true });
      } else {
        return _react2.default.createElement(
          "div",
          null,
          _react2.default.createElement("img", { style: style,
            onClick: this.clickHandler.bind(this),
            className: className,
            src: "https://img.youtube.com/vi/" + link + "/0.jpg" })
        );
      }
    }
  }]);

  return YoutubeLazy;
}(_react2.default.Component);

// <img style={styleTwo}
// className={className + " youtube"}
// src={"../static/svg/youtube-play-button.svg"} >
// </img>


exports.default = YoutubeLazy;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(YoutubeLazy, "YoutubeLazy", "app/frontend/components/organisms/youTubeLazyLoader.js");
}();

;