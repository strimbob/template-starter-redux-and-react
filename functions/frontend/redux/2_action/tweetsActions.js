"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchTweets = fetchTweets;
exports.addTweet = addTweet;
exports.updateTweet = updateTweet;
exports.deleteTweet = deleteTweet;

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function fetchTweets() {
  return function (dispatch) {
    dispatch({ type: "FETCH_TWEETS" });

    /* 
      http://rest.learncode.academy is a public test server, so another user's experimentation can break your tests
      If you get console errors due to bad data:
      - change "reacttest" below to any other username
      - post some tweets to http://rest.learncode.academy/api/yourusername/tweets
    */
    _axios2.default.get("http://rest.learncode.academy/api/reacttest/tweets").then(function (response) {
      dispatch({ type: "FETCH_TWEETS_FULFILLED", payload: response.data });
    }).catch(function (err) {
      dispatch({ type: "FETCH_TWEETS_REJECTED", payload: err });
    });
  };
}

function addTweet(id, text) {
  return {
    type: 'ADD_TWEET',
    payload: {
      id: id,
      text: text
    }
  };
}

function updateTweet(id, text) {
  return {
    type: 'UPDATE_TWEET',
    payload: {
      id: id,
      text: text
    }
  };
}

function deleteTweet(id) {
  return { type: 'DELETE_TWEET', payload: id };
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(fetchTweets, "fetchTweets", "app/frontend/redux/2_action/tweetsActions.js");

  __REACT_HOT_LOADER__.register(addTweet, "addTweet", "app/frontend/redux/2_action/tweetsActions.js");

  __REACT_HOT_LOADER__.register(updateTweet, "updateTweet", "app/frontend/redux/2_action/tweetsActions.js");

  __REACT_HOT_LOADER__.register(deleteTweet, "deleteTweet", "app/frontend/redux/2_action/tweetsActions.js");
}();

;