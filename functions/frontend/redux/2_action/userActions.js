"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchUser = fetchUser;
exports.setUserName = setUserName;
exports.setUserAge = setUserAge;
function fetchUser() {
  return {
    type: "FETCH_USER_FULFILLED",
    payload: {
      name: "Will",
      age: 35
    }
  };
}

function setUserName(name) {
  return {
    type: 'SET_USER_NAME',
    payload: name
  };
}

function setUserAge(age) {
  return {
    type: 'SET_USER_AGE',
    payload: age
  };
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(fetchUser, "fetchUser", "app/frontend/redux/2_action/userActions.js");

  __REACT_HOT_LOADER__.register(setUserName, "setUserName", "app/frontend/redux/2_action/userActions.js");

  __REACT_HOT_LOADER__.register(setUserAge, "setUserAge", "app/frontend/redux/2_action/userActions.js");
}();

;