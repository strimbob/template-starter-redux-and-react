"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = reducer;
function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    pageData: {
      id: null,
      title: null,
      subtitle: null,
      textBody: [[]],
      imgPhone: null,
      imgDtop: null,
      imgPhoneS: null,
      imgDtopS: null,
      hero: null,
      iframes: [],
      pageImage: null,
      numberImage: null
    },
    fetching: false,
    fetched: false,
    error: null
  };
  var action = arguments[1];


  switch (action.type) {
    case "FETCHING_PAGE":
      {
        return _extends({}, state, { fetching: true });
      }
    case "FETCHED_PAGED":
      {
        return _extends({}, state, { fetching: false, fetched: true, pageData: action.payload });
      }
    case "FETCHING_ERROR":
      {
        return _extends({}, state, { fetching: false, fetched: false, error: "page not found 404 " });
      }
  }
  return state;
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(reducer, "reducer", "app/frontend/redux/3_reducers/pageDataReducer.js");
}();

;