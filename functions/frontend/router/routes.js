'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _download = require('../components/pages/download.js');

var _download2 = _interopRequireDefault(_download);

var _index = require('../components/pages/index.js');

var _index2 = _interopRequireDefault(_index);

var _tutorials = require('../components/pages/tutorials.js');

var _tutorials2 = _interopRequireDefault(_tutorials);

var _appRoot = require('./app-root');

var _appRoot2 = _interopRequireDefault(_appRoot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var routes = [{ component: _appRoot2.default,
  routes: [{ path: '/',
    exact: true,
    component: _index2.default
  }, { path: '/Download',
    component: _download2.default
  }, { path: '/Tutorials',
    component: _tutorials2.default
  }]
}];
var _default = routes;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(routes, 'routes', 'app/frontend/router/routes.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/router/routes.js');
}();

;