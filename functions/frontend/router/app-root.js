"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouterConfig = require("react-router-config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AppRoot = function AppRoot(props) {
  return _react2.default.createElement(
    "div",
    null,
    (0, _reactRouterConfig.renderRoutes)(props.route.routes)
  );
};

var _default = AppRoot;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(AppRoot, "AppRoot", "app/frontend/router/app-root.js");

  __REACT_HOT_LOADER__.register(_default, "default", "app/frontend/router/app-root.js");
}();

;