'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require('react-router-dom');

var _reactRouterConfig = require('react-router-config');

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Root = function Root() {
  return _react2.default.createElement(
    _reactRouterDom.BrowserRouter,
    { basename: process.env.PUBLIC_URL },
    (0, _reactRouterConfig.renderRoutes)(_routes2.default)
  );
};

var _default = Root;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(Root, 'Root', 'app/frontend/router/index.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'app/frontend/router/index.js');
}();

;