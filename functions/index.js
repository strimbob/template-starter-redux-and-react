'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.daBrain = undefined;

var _firebaseFunctions = require('firebase-functions');

var functions = _interopRequireWildcard(_firebaseFunctions);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _server = require('react-dom/server');

var _StaticRouter = require('react-router-dom/StaticRouter');

var _StaticRouter2 = _interopRequireDefault(_StaticRouter);

var _reactRouterConfig = require('react-router-config');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _routes = require('./frontend/router/routes.js');

var _routes2 = _interopRequireDefault(_routes);

var _index2 = require('./frontend/components/pages/index.js');

var _index3 = _interopRequireDefault(_index2);

var _reactRedux = require('react-redux');

var _store = require('./frontend/redux/1_store/store.js');

var _store2 = _interopRequireDefault(_store);

var _reactResponsiveRedux = require('react-responsive-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var firebase = require('firebase-admin');
var firebaseApp = firebase.initializeApp(functions.config().firebase);
var database = firebase.database();


var path = require("path");

var app = (0, _express2.default)();

var assets = _express2.default.static(path.join(__dirname, '../public'));
app.use(assets);
var _index = _fs2.default.readFileSync(path.resolve(__dirname, './public', 'nerves.html'), 'utf8');

function renderFullPage(html, preloadedState) {
  var htmlFinal = _index.replace("<!--- ::APP::----->", html);
  var store_init = '<script> window.__PRELOADED_STATE__ = ' + JSON.stringify(preloadedState).replace(/</g, '\\u003c') + ' </script>';
  htmlFinal = htmlFinal.replace("<!--- ::store::----->", store_init);
  return htmlFinal;
}

app.get('*', function (request, response) {

  var context = {};
  var dispatch = _store2.default.dispatch;

  // do our mobile detection

  var mobileDetect = (0, _reactResponsiveRedux.mobileParser)(request);
  dispatch((0, _reactResponsiveRedux.setMobileDetect)(mobileDetect));

  var content = (0, _server.renderToString)(_react2.default.createElement(
    _reactRedux.Provider,
    { store: _store2.default },
    _react2.default.createElement(
      _StaticRouter2.default,
      { location: request.url, context: context },
      (0, _reactRouterConfig.renderRoutes)(_routes2.default)
    )
  ));

  var htmlFinal = _index.replace("<!--- ::APP::----->", content);
  var preloadedState = _store2.default.getState();
  response.send(renderFullPage(content, preloadedState));
});

var daBrain = exports.daBrain = functions.https.onRequest(app);
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(firebaseApp, 'firebaseApp', 'index.js');

  __REACT_HOT_LOADER__.register(database, 'database', 'index.js');

  __REACT_HOT_LOADER__.register(app, 'app', 'index.js');

  __REACT_HOT_LOADER__.register(assets, 'assets', 'index.js');

  __REACT_HOT_LOADER__.register(_index, '_index', 'index.js');

  __REACT_HOT_LOADER__.register(renderFullPage, 'renderFullPage', 'index.js');

  __REACT_HOT_LOADER__.register(daBrain, 'daBrain', 'index.js');
}();

;